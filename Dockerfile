# Completez le Dockerfile afin de faire fonctionner le serveur Rails
FROM ruby:2.5

RUN apt update -qq && apt install -y nodejs postgresql-client
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install
COPY . /app

VOLUME ./db /db
VOLUME ./storage /storage

# Conservez les lignes ci-dessous
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# N'oubliez pas la commande pour démarrer le serveur
EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
